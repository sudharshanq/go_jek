/**
 * Created by sudharshanr on 1/7/2017.
 */
module.exports= function PaymentPage() {
    var paymentPageTitile =element(by.id('PaymentPage'));
    var creditCardTab = element(by.id('CCTab'));
    var debitCardTab = element(by.id('DCTab'));
    this.waitUntilPaymentPageLoads = function () {
        browser.wait(function () {
            return paymentPageTitile.isPresent();
        },20000)
    };
    this.isPaymentTitleIsPresent = function () {
        return paymentPageTitile.isPresent();
    };
    this.isCreditCardTabIsPresent = function () {
        return creditCardTab.isPresent();
    };
    this.isDebitCardTabIsPresent = function () {
        return debitCardTab.isPresent();
    }
};