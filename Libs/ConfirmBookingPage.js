/**
 * Created by sudharshanr on 1/7/2017.
 */
/**
 * Created by sudharshanr on 1/6/2017.
 */
module.exports= function ConfirmBookingPage() {
    var firstTripDetails = element(by.css('.onwBlock h1'));
    var returnTripDetails = element(by.css('.retBlock h1'));
    var confirmBookingButton = element(by.id('itineraryBtn'));
    var insuranceCheckBox = element(by.id('insurance_box'));

    this.getFirstTripDetails = function () {
        browser.wait(function () {
            return firstTripDetails.isPresent()
        },10000);
        return firstTripDetails.getText();
    };
    this.getReturnTripDetails = function () {
        browser.wait(function () {
            return returnTripDetails.isPresent()
        },10000);
        return returnTripDetails.getText();
    };
    this.clickOnConfirmBooking = function () {
        return confirmBookingButton.click();
    };
    this.uncheckInsuranceCheckBox = function () {
        if((insuranceCheckBox.isSelected()==true)){
            return insuranceCheckBox.click();
        }
    };

};
