/**
 * Created by sudharshanr on 1/7/2017.
 */
module.exports= function SearchFlights(){
    var flightOption = element(by.css('.hasProductIcons .productNav .flightApp'))
    var onewayRadioButton = element(by.id('OneWay'));
    var roundTripRadioButton = element(by.id('RoundTrip'));
    var multicityRadioButton = element(by.id('MultiCity'));
    var flightAndHotelRadioButton = element(by.id('Packages'));
    var fromCity = element(by.id('FromTag'));
    var toCity = element(by.id('ToTag'));
    var departDateCalender = element(by.id('DepartDate'));
    var returnDateCalender = element(by.id('ReturnDate'));
    var firstMonthName = element(by.css('.monthBlock.first .title .ui-datepicker-month'));
    var firstMonthYear = element(by.css('.monthBlock.first .title .ui-datepicker-year'));
    var secondMonthName = element(by.css('.monthBlock.last .title .ui-datepicker-month'));
    var secondMonthYear = element(by.css('.monthBlock.last .title .ui-datepicker-year'));
    var nextMonthArrow = element(by.css('.nextMonth'));
    var prevMonthArrow = element(by.css('.prevMonth'));
    var adultsDropDown = element(by.id('Adults'));
    var childresnDropDown = element(by.id('Childrens'));
    var infantsDropDown = element(by.id('Infants'));
    var searchButton = element(by.id('SearchBtn'));

    this.isflightOptionIsPresent = function () {
        browser.wait(function () {
            return flightOption.isPresent()
        },50000);
        return lightOption.isPresent();
    };
    this.clickOnFightOption = function () {
        browser.wait(function () {
            return flightOption.isDisplayed()
        },10000);
        return flightOption.click();
    };
    this.clickOnRoundTripRadioButton = function () {
        browser.wait(function () {
            return roundTripRadioButton.isDisplayed()
        },10000);
        return roundTripRadioButton.click();
    };
    this.selectFromAirport = function (name) {
        fromCity.sendKeys(name);
        return element(by.cssContainingText('.list a',name)).click();
    };
    this.selectToCityAirport = function (name) {
        toCity.sendKeys(name);
        return element(by.cssContainingText('.list a',name)).click();
    };
    this.selectFirstMonthDate = function (date) {
        return element(by.cssContainingText('.monthBlock.first .calendar  .ui-state-default',date)).click();
    };
    this.selectSecondMonthDate = function (date) {
        return element(by.cssContainingText('.monthBlock.last .calendar  .ui-state-default',date)).click();
    };
    this.selectDepartDate = function (date,month,year) {
        departDateCalender.click();
        while(firstMonthName.getText ==month || secondMonthName.getText==month ){
            if(firstMonthName.getText==month){
                if(firstMonthYear==year){
                    this.selectFirstMonthDate(date);
                    break;
                }
            }else if(secondMonthName.getText==month){
                if(secondMonthYear.getText==year){
                    this.selectSecondMonthDate(date);
                    break;
                }
            }
            nextMonthArrow.click();
        }

    };
    this.selectReturnDate = function (date,month,year) {
        returnDateCalender.click();
        while(firstMonthName.getText==month || secondMonthName.getText==month){
            if(firstMonthName.getText==month){
                if(firstMonthYear==year){
                    this.selectFirstMonthDate(date);
                    break;
                }
            }else if(secondMonthName.getText==month){
                if(secondMonthYear.getText==year){
                    this.selectSecondMonthDate(date);
                    break;
                }
            }
            nextMonthArrow.click();
        }
    };
    this.selectAdultsCount = function (count) {
        adultsDropDown.click();
        browser.wait(function () {
            return element(by.css('#Adults option')).isDisplayed()
        },5000);
        return element(by.cssContainingText('#Adults option[value="'+count+'"]')).click();
    };
    this.selectChildrenCount = function (count) {
        childresnDropDown.click();
        browser.wait(function () {
            return element(by.css('#Childrens option')).isDisplayed()
        },5000);
        return element(by.cssContainingText('#Childrens option[value="'+count+'"]')).click();
    };
    this.selectInfantsCount = function () {
        infantsDropDown.click();
        browser.wait(function () {
            return element(by.css('#Infants option')).isDisplayed()
        },5000);
        return element(by.cssContainingText('#Infants option[value="'+count+'"]')).click();
    };
    this.clickOnSearchButton = function () {
        searchButton.click();
    };
};