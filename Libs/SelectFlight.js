/**
 * Created by sudharshanr on 1/7/2017.
 */
module.exports= function SelectFlight() {
    var pageLoadElement = element(by.css('.pageLoader.relative.squeezeMe'));
    var fromAndToCityNamesOfFirstTrip = element(by.css('.colZero.leg[data-leg="1"] .listTitle strong'));
    var fromAndToCityNamesOfSecondTrip = element(by.css('.colZero.leg[data-leg="2"] .listTitle strong'));
    var departureDate = element(by.css('.colZero.leg[data-leg="1"] .listTitle span'));
    var returnDate = element(by.css('.colZero.leg[data-leg="2"] .listTitle span'));
    var bookButton = element(by.css('.resultContainer .booking.fRight'));
    this.waitToPageLoad = function () {
        browser.wait(function () {
            return pageLoadElement.isPresent();
        },50000)
    };
    this.getCitiesNamesOfFromAndToOfFirstTrip = function () {
        return fromAndToCityNamesOfFirstTrip.getText();
    };
    this.getCitiesNamesOfFromAndToOfSeconTrip = function () {
        return fromAndToCityNamesOfSecondTrip.getText();
    };
    this.getDepartureDate = function () {
        return departureDate.getText();
    };
    this.getReturnDate = function () {
        return returnDate.getText();
    };
    this.clickOnBookButton = function () {
        return bookButton.click();
    }
};