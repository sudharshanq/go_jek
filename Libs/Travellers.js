/**
 * Created by sudharshanr on 1/7/2017.
 */
module.exports= function  Travellers(){
    var travellerPageHeader =element(by.id('travellerOpen'));
    var mobileNumberField =element(by.id('mobileNumber'));
    var countinueButton = element(by.id('travellerBtn'));

    this.waitUntilTravellerPageLoad = function () {
        browser.wait(function () {
            return travellerPageHeader.isPresent();
        },20000)
    }
    this.selectAdultTitle = function (i,title) {
        element(by.css('#travellerBlock #AdultTitle'+i)).click();
        element(by.css('#travellerBlock #AdultTitle'+i+' option[value="'+title+'"]')).click();
    };
    this.enterAdultFirstName = function (i,name) {
        return element(by.css('#travellerBlock #AdultFname'+i)).sendKeys(name)
    };
    this.enterAdultLastName = function (i,name) {
        return element(by.css('#travellerBlock #AdultLname'+i)).sendKeys(name)
    };
    this.selectChildTitle = function (i,title) {
        element(by.css('#ChildTitle'+i)).click();
        browser.wait(function () {
            return element(by.css('#ChildTitle'+i+' option[value="Miss"]')).isDisplayed()
        },2000);
        return element(by.css('#ChildTitle'+i+' option[value="Miss"]'))
    };
    this.enterChildFirstName = function (i,name) {
        return element(by.css('#ChildFname'+i)).sendKeys(name)
    };
    this.enterChildSecondName = function (i,name) {
        return element(by.css('#ChildLname'+i)).sendKeys(name);
    };
    this.enterChildBirthDate = function (i,day) {
        element(by.css('#ChildDobDay'+i)).click();
        browser.wait(function () {
            return element(by.css('#ChildDobDay'+i+' option')).isDisplayed()
        },2000);
        return element(by.css('#ChildDobDay'+i+' option[value="'+day+'"]')).click();
    };
    this.enterChildBirthMonth= function (i,month) {
        element(by.css('#ChildDobMonth'+i)).click();
        browser.wait(function () {
            return element(by.css('#ChildDobMonth'+i+' option')).isDisplayed()
        },2000);
        return element(by.css('#ChildDobMonth'+i+' option[value="'+month+'"]')).click();
    };
    this.enterChildBirthYear = function (i,year) {
        element(by.css('#ChildDobYear'+i)).click();
        browser.wait(function () {
            return element(by.css('#ChildDobYear'+i+' option')).isDisplayed()
        },2000);
        return element(by.css('#ChildDobYear'+i+' option[value="'+year+'"]')).click();
    };
    this.enterMobileNumber = function (number) {
        return mobileNumberField.sendKeys(number);
    };
    this.clickCountinueButton = function () {
        return countinueButton.click();
    };
    this.enterPassengersDetails = function (data) {
        var len1 =data.adult.length;
        var len2 = data.child.length;
        for(var i=0;i<len1;i++){

            this.selectAdultTitle(i+1,data.adult[i].title);
            this.enterAdultFirstName(i+1,data.adult[i].fname);
            this.enterAdultLastName(i+1,data.adult[i].lname);
        }
        for(var j=0;j<len2;j++){
            this.selectAdultTitle(j+1,data.child[j].title);
            this.enterChildFirstName(j+1,data.child[j].fname);
            this.enterChildSecondName(j+1,data.child[j].lname);
            this.enterChildBirthDate(j+1,data.child[j].day);
            this.enterChildBirthMonth(j+1,data.child[j].month);
            this.enterChildBirthYear(j+1,data.child[j].year)
        }
    }
};