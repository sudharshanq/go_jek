/**
 * Created by sudharshanr on 1/7/2017.
 */

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    suites :{
        ClearTrip:[

            '../Tests/ClearTrip_round_trip_flight_booking.js'

        ]
    }
};