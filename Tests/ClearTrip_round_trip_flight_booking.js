/**
 * Created by sudharshanr on 1/7/2017.
 */
var SearchFlightPage= require('../Libs/SearchFlights.js');
var SelectFlightPage= require('../Libs/SelectFlight.js');
var ConfirmBooking = require('../Libs/ConfirmBookingPage.js');
var Payment =require('../Libs/PaymentPage.js');
var TravellersPage = require('../Libs/Travellers.js');

describe('ClearTrip_round_trip_flight_booking', function (){
    var runId = (new Date().getTime()).toString();
    var fromCity= 'Bangalore';
    var toCity ='Hyderabad';
    var passengers = {
        'adult':[{'title':'Mr','fname':'fname1'+runId.substring(6),'lname':'lname1'+runId.substring(6)},
            {'title':'Ms','fname':'fname2'+runId.substring(6),'lname':'lname2'+runId.substring(6)}
        ],
        'child':[{'title':'Miss','fname':'fname3'+runId.substring(6),'lname':'lname3'+runId.substring(6),'day':'3','month':'Apr','year':'2002'}]

    };
    var searchFlightPage = new SearchFlightPage();
    var selectFlightPage = new SelectFlightPage();
    var confirmBookingPage = new ConfirmBooking();
    var travellersPage = new TravellersPage();
    var paymentPage = new Payment();
    it('Launch cleartrip website', function () {
        browser.ignoreSynchronization = true;
        browser.get('https://www.cleartrip.com/');
    });
    it('Wait until page loads', function () {
        browser.ignoreSynchronization = true;
        searchFlightPage.isflightOptionIsPresent().then(function (state) {
            expect('Page load completed:'+state).toBe('Page load completed:true')
        })
    });
    it('Select flight option and select round trip option', function () {
        searchFlightPage.clickOnFightOption();
        searchFlightPage.clickOnRoundTripRadioButton();
    });
    it('Enter from and to city airports', function () {
        searchFlightPage.selectFromAirport(fromCity);
        searchFlightPage.selectToCityAirport(toCity);
    });
    it('Enter Departure and return dates', function () {
        searchFlightPage.selectDepartDate('17','January','2017');
        searchFlightPage.selectReturnDate('19','January','2017');
    });
    it('Select adults , child and infants count', function () {
        searchFlightPage.selectAdultsCount(passengers.adult.length);
        searchFlightPage.selectChildrenCount(passengers.child.length);
    });
    it('Click on search flights button', function () {
        searchFlightPage.clickOnSearchButton();
    });
    it('Wait until to fetch flight data', function () {
        selectFlightPage.waitToPageLoad();
    });
    it('Verify from and to cities and date of journey', function () {
        selectFlightPage.getCitiesNamesOfFromAndToOfFirstTrip().then(function (text) {
            expect(text).toBe(fromCity+' → '+toCity);
        });
        selectFlightPage.getCitiesNamesOfFromAndToOfSeconTrip().then(function (text) {
            expect(text).toBe(toCity+' → '+fromCity);
        });
        selectFlightPage.getDepartureDate().then(function (text) {
            expect(text).toContain('17');
            expect(text).toContain('Jan');
        });
        selectFlightPage.getReturnDate().then(function (text) {
            expect(text).toContain('19');
            expect(text).toContain('Jan');
        })
    });
    // here im unable to select flights because its dynamic data , Selecting default flights
    it('Click on book button ', function () {
        selectFlightPage.clickOnBookButton();
    });
    it('Verify journey details ', function () {
        confirmBookingPage.getFirstTripDetails().then(function (text) {
            expect(text).toBe(fromCity+' → '+toCity);
        });
        confirmBookingPage.getReturnTripDetails().then(function () {
            expect(text).toBe(toCity+' → '+fromCity);
        })
    });
    it('Uncheck insurance check box and click on confirm booking', function () {
        confirmBookingPage.uncheckInsuranceCheckBox();
        confirmBookingPage.clickOnConfirmBooking();
    });
    it('Wait until travellers page load', function () {
        travellersPage.waitUntilTravellerPageLoad();
    });
    it('Enter passengers details', function () {
        travellersPage.enterPassengersDetails(passengers);
    });
    it('Enter mobile number and click continue', function () {
        travellersPage.enterMobileNumber('8888888888');
        travellersPage.clickCountinueButton();
    });
    it('Wait until page loads', function () {
        paymentPage.waitUntilPaymentPageLoads()
    });
    it('verify payment page is appearing', function () {
        paymentPage.isCreditCardTabIsPresent().then(function (state) {
            expect(state).toBe(true);
        });
        paymentPage.isDebitCardTabIsPresent().then(function(state){
            expect(state).toBe(true);
        })
    })
});
